package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import org.w3c.dom.Text
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {
    private lateinit var S: EditText
    private lateinit var D: EditText
    private lateinit var yee: Button
    private lateinit var w: TextView

    private var yeeCheckOnClick = View.OnClickListener {
        var S = S.text.toString().toDoubleOrNull()
        S?.let {
        }
        var D = D.text.toString().toDoubleOrNull()
        D?.let {
        }
        if (S != null && D != null) {
            var g = S*15 + D*30
            w.text = w.toString()
            var sad = DecimalFormat("#")
            w.text = sad.format(g)
        }
}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        S = findViewById(R.id.S)
        D = findViewById(R.id.D)
        yee =  findViewById(R.id.yee)
        w = findViewById(R.id.w)
        yee.setOnClickListener(yeeCheckOnClick)
    }
}